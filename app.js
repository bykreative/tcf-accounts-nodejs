const axios = require("axios");

module.exports = {
    expressMiddlewareAuthenticate: (req, res, next) => {
        const usernameGiven = req.body.username;
        const authToken = req.headers.authToken;
        if (authToken === null) {
            res.status(202).json({
                statusCode: 401,
                timestamp: Date.now(),
                message: "auth token missing",
                data: {
                    errorCode: "missing_token",
                    givenUsername: usernameGiven
                }
            });
        }
        else {
            axios.post("https://accounts-api.tcff.co/auth/verify", {
                username: usernameGiven,
                token: authToken
            }).then((response) => {
                if (response.data.statusCode === 505) {
                    res.status(202).json({
                        statusCode: 505,
                        timestamp: Date.now(),
                        message: "authentication failed with unknown error",
                        data: {
                            error: response.data.data,
                            errorCode: "system_failure",
                            givenUsername: usernameGiven
                        }
                    });
                }
                else if (response.data.statusCode === 403) {
                    res.status(202).json({
                        statusCode: 403,
                        timestamp: Date.now(),
                        message: "authentication failed",
                        data: {
                            error: response.data.data,
                            errorCode: "unauthenticated",
                            givenUsername: usernameGiven
                        }
                    });
                }
                else {
                    req.tcfAccount = response.data.data;
                    next();
                }
            }).catch((error) => {
                res.status(202).json({
                    statusCode: 505,
                    timestamp: Date.now(),
                    message: "unknown error",
                    data: {
                        error: error,
                        errorCode: "system_failure",
                        givenUsername: usernameGiven
                    }
                });
            });
        }
    }
};